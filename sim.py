#!/usr/bin/env python


import csv
import math


# constants
debug = False
init_liquidity = 10000.0
trade_profit = 0.0
write = True


# classes
class Asset:

    def __eq__(self, other):
        return self.name == other.name

    def __init__(self, name, denorm_weight=1):
        self.name = name
        self.count = 0.0
        self.price = 0.0
        self.weight = denorm_weight
        self.value = 0.0

    def add_to_count(self, add_count):
        self.set_count(self.count + add_count)

    def set_count(self, new_count):
        self.count = new_count
        self.set_value()

    def set_price(self, new_price):
        self.price = new_price
        self.set_value()

    def set_value(self):
        self.value = self.count * self.price


class Portfolio:

    def __init__(self, daily_volume_pct, trade_fee):
        self.assets = {}
        self.daily_volume_pct = daily_volume_pct
        self.trade_fee = trade_fee
        self.trades = 0

    def add_asset(self, asset, denorm_weight=1):
        if isinstance(asset, str):
            self.assets[asset] = Asset(asset, denorm_weight)
        else:
            self.assets[asset.name] = asset

    def compute_in_given_out(self, asset_in, asset_out, count_out):
        return asset_in.count \
                * (math.pow(asset_out.count / (asset_out.count - count_out), \
                asset_out.weight / asset_in.weight) - 1.0) \
                * (1.0 / (1.0 - self.trade_fee))

    def compute_in_given_price(self, asset_in, asset_out):
        market_price = self.compute_spot_price(asset_in, asset_out)
        redemption_price = self.compute_redemption_price(asset_in, asset_out)
        return asset_in.count \
                * (math.pow(redemption_price / market_price, \
                asset_out.weight / (asset_out.weight + asset_in.weight)) - 1.0)

    def compute_out_given_in(self, asset_in, asset_out, count_in):
        return asset_out.count * (1.0 - math.pow(asset_in.count \
                / (asset_in.count + count_in * (1.0 - self.trade_fee)), \
                asset_in.weight / asset_out.weight))

    def compute_redemption_price(self, asset_in, asset_out):
        return (asset_out.price / asset_in.price)

    def compute_spot_price(self, asset_in, asset_out):
        return (asset_in.count / asset_in.weight) \
                / (asset_out.count / asset_out.weight)

    def get_liquidity(self):
        liquidity = 0
        for key, asset in self.assets.items():
            liquidity += asset.value
        return liquidity

    def rebalance(self):
        assets = list(self.assets.values())
        n = len(assets)
        for i in range(0, n):
            for j in range(i+1, n):
                self.trade(assets[i], assets[j])

    def set_prices(self, prices):
        for key, asset in self.assets.items():
            asset.set_price(float(prices[key]))

    def trade(self, asset1, asset2):
        # compute relative pricing on both markets (internal and external)
        # price is asset2 in terms of asset1
        market_price = self.compute_spot_price(asset1, asset2)
        redemption_price = self.compute_redemption_price(asset1, asset2)

        # the more valuable asset (to redeem externally) is asset_out
        if redemption_price > market_price:
            asset_in = asset1
            asset_out = asset2
        else:
            asset_in = asset2
            asset_out = asset1

        # compute trade size required for equalizing arbitrage
        amount_in = self.compute_in_given_price(asset_in, asset_out)
        amount_out = self.compute_out_given_in(asset_in, asset_out, amount_in)

        # compute in/out in terms of USD for the arbitrager
        value_in = amount_in * asset_in.price
        value_out = amount_out * asset_out.price

        if debug:
            print('trade fee = %.2f' % self.trade_fee)
            print('asset1 balance = %.2f' % asset1.count)
            print('asset2 balance = %.2f' % asset2.count)
            print('asset1 redemption price (USD) = %.2f' % asset1.price)
            print('asset2 redemption price (USD) = %.2f' % asset2.price)
            print('relative market price = %.2f' % market_price)
            print('relative redemption price = %.2f' % redemption_price)
            print('amount in = %.2f' % amount_in)
            print('amount out = %.2f' % amount_out)
            print('value in (USD) = %.2f' % value_in)
            print('value out (USD) = %.2f' % value_out)

        # only simulate arbitrage above a fixed USD profit
        if value_out - value_in > trade_profit:
            self.trades += 1
            asset_in.add_to_count(amount_in)
            asset_out.add_to_count(-amount_out)
            if debug:
                print('trading...')
                print('asset in count = %.2f' % asset_in.count)
                print('asset out count = %.2f' % asset_out.count)

        # add fixed volume per hour to simulate liquid trading beyond arbitrage
        # NOTE This is an approximation of 'hourly' volume based on 1/24th
        #      daily volume. It compounds, so it really ends up being a tad
        #      larger than daily volume (lazy math)
        hourly_volume = self.daily_volume_pct * self.get_liquidity() / 24.0
        asset1.add_to_count(hourly_volume * self.trade_fee/2.0 / asset1.price)
        asset2.add_to_count(hourly_volume * self.trade_fee/2.0 / asset2.price)

        if debug:
            input()

    def update_weights(self):
        sum_weights = 0
        for key, asset in self.assets.items():
            sum_weights += asset.weight
        for key, asset in self.assets.items():
            asset.weight /= sum_weights


# loop
for eth_denorm_weight in [ 0.0, 0.5, 1.0 ]:

    for trade_fee in [ 0.0, 0.001, 0.002, 0.003, 0.01, 0.05, 0.1 ]:

        for daily_volume_pct in [ 0.0, 0.01, 0.05, 0.1 ]:

            # omit liquid trading for 50/50 Set markets (no one will use this)
            if daily_volume_pct > 0.0 and eth_denorm_weight is 0.0:
                continue

            # omit liquid trading for high fee pools
            if daily_volume_pct > 0.0 and trade_fee >= 0.01:
                continue

            # omit liquid trading for zero-fee pools (it makes no difference)
            if daily_volume_pct > 0.0 and trade_fee is 0.0:
                continue

            # omit Set/Set/ETH pools with high trade fees (ETH adds extra
            # liquidity that precludes the need for high fees)
            if eth_denorm_weight > 0.0 and trade_fee >= 0.01:
                continue

            # omit Set/Set/ETH pools with low volume (the point of adding ETH
            # is to attract volume)
            if eth_denorm_weight > 0.0 and daily_volume_pct < 0.01:
                continue

            # initialize the unchanging reference portfolio
            init_portfolio = Portfolio(daily_volume_pct, trade_fee)
            if eth_denorm_weight > 0.0:
                init_portfolio.add_asset('ETH', eth_denorm_weight)
                init_portfolio.add_asset('ETHBTCRSI')
                init_portfolio.add_asset('ETHUSDRSI')
            else:
                init_portfolio.add_asset('ETHBTCRSI')
                init_portfolio.add_asset('ETHUSDRSI')
            init_portfolio.update_weights()

            # initialize HODL positions for ETH and BTC
            btc = Asset('BTC')
            eth = Asset('ETH')

            # initialize the rebalancing portfolio
            portfolio = Portfolio(daily_volume_pct, trade_fee)
            if eth_denorm_weight > 0.0:
                portfolio.add_asset('ETH', eth_denorm_weight)
                portfolio.add_asset('ETHBTCRSI')
                portfolio.add_asset('ETHUSDRSI')
            else:
                portfolio.add_asset('ETHBTCRSI')
                portfolio.add_asset('ETHUSDRSI')
            portfolio.update_weights()

            first_pass = True

            # open CSV for reading (hourly price data)
            with open('setofsets_ethbtcusd.csv') as f:

                reader = csv.DictReader(f)

                # open output CSV for writing
                filename = 'setofsets_ethbtcusd_out_' \
                    if eth_denorm_weight > 0.0 else 'setofsets_btcusd_out_'
                with open('setofsets_out_eth' + str(eth_denorm_weight) + '_fee'
                        + str(trade_fee) + '_vol' + str(daily_volume_pct)
                        + '.csv', mode='w') as fout:

                    # initialize CSV header for hourly output data
                    if write:
                        writer = csv.writer(fout, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        writer.writerow(['SECONDS', 'ETH', 'BTC', 'ETHUSDRSI',
                                'ETHBTCRSI', 'BP'])

                    # read hourly price data from input CSV
                    for row in reader:

                        # update redemption prices
                        btc.set_price(float(row['BTC']))
                        eth.set_price(float(row['ETH']))
                        init_portfolio.set_prices(row)
                        portfolio.set_prices(row)

                        # initialize portfolios according to weights
                        if first_pass:

                            if eth_denorm_weight > 0.0:
                                init_portfolio.assets['ETH'].set_count(
                                        init_portfolio.assets['ETH'].weight
                                        * init_liquidity / float(row['ETH']))
                            init_portfolio.assets['ETHBTCRSI'].set_count(
                                    init_portfolio.assets['ETHBTCRSI'].weight
                                    * init_liquidity / float(row['ETHBTCRSI']))
                            init_portfolio.assets['ETHUSDRSI'].set_count(
                                    init_portfolio.assets['ETHUSDRSI'].weight
                                    * init_liquidity / float(row['ETHUSDRSI']))

                            btc.set_count(init_liquidity / float(row['BTC']))
                            eth.set_count(init_liquidity / float(row['ETH']))

                            if eth_denorm_weight > 0.0:
                                portfolio.assets['ETH'].set_count(
                                        portfolio.assets['ETH'].weight
                                        * init_liquidity / float(row['ETH']))
                            portfolio.assets['ETHBTCRSI'].set_count(
                                    portfolio.assets['ETHBTCRSI'].weight
                                    * init_liquidity / float(row['ETHBTCRSI']))
                            portfolio.assets['ETHUSDRSI'].set_count(
                                    portfolio.assets['ETHUSDRSI'].weight
                                    * init_liquidity / float(row['ETHUSDRSI']))

                            first_pass = False

                        # or, rebalance the existing portfolio
                        else:

                            portfolio.rebalance()

                        # write hourly output data for the pool
                        if write:
                            writer.writerow([
                                float(row['SECONDS']), eth.value, btc.value,
                                init_portfolio.assets['ETHUSDRSI'].value
                                / init_portfolio.assets['ETHUSDRSI'].weight,
                                init_portfolio.assets['ETHBTCRSI'].value
                                / init_portfolio.assets['ETHBTCRSI'].weight,
                                portfolio.get_liquidity()])

            # print user-friendly output to shell
            print('\tBP value (ETH=%.2f, fee = %.2f%%, trades = %d, volume = %.2f%%) = %.2f'
                    % (eth_denorm_weight, 100.0*trade_fee, portfolio.trades,
                    100.0*daily_volume_pct, portfolio.get_liquidity()))

# generate HODL comparison data
ethbtcset = init_portfolio.assets['ETHBTCRSI'].value \
        / init_portfolio.assets['ETHBTCRSI'].weight
ethusdset = init_portfolio.assets['ETHUSDRSI'].value \
        / init_portfolio.assets['ETHUSDRSI'].weight
set50 = (ethbtcset + ethusdset) / 2.0
all33 = (eth.value + ethbtcset + ethusdset) / 3.0

# print HODL comparison dat
print('\tETH value = %.2f' % eth.value)
print('\tBTC value = %.2f' % btc.value)
print('\tETHBTCRSI value = %.2f' % (init_portfolio.assets['ETHBTCRSI'].value
        / init_portfolio.assets['ETHBTCRSI'].weight))
print('\tETHUSDRSI value = %.2f' % (init_portfolio.assets['ETHUSDRSI'].value
        / init_portfolio.assets['ETHUSDRSI'].weight))
print('\t50/50 Set value = %.2f' % set50)
print('\t33/33/33 value = %.2f' % all33)
print('\tUSD value = %.2f' % init_liquidity)
